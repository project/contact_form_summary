CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This allows you to display a modal after contact form's submission.
It will display a resume of what the user send.

REQUIREMENTS
------------

This module requires the following modules:
 * Contact (Core)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8 for
   further information.

CONFIGURATION
-------------

* Enable a contact form's summary modal:
  - When creating or editing a contact form.
  - Check 'Activate post submit summary'.
  - You must set a modal's title when modal is enabled.

* Edit form's summary:
  - Go to form's 'Manage display''s page.
  - Create a view mode with machine name 'modal'.
  - Edit view mode as you wish.

MAINTAINERS
-----------

Current maintainers:
 * Quentin Decaunes (qudec) - https://www.drupal.org/user/3543687
 * Benjamin Rambaud (beram) - https://www.drupal.org/user/3508624
 * Florent Torregrosa (Grimreaper) - https://www.drupal.org/user/2388214

This project has been sponsored by:
 * Smile - http://www.smile.fr
